package com.example.exercise07.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Adapter;

import com.bumptech.glide.Glide;
import com.example.exercise07.R;
import com.example.exercise07.adapter.NewsAdapter;
import com.example.exercise07.interfaces.NewsOnClick;
import com.example.exercise07.model.Item;
import com.example.exercise07.network.APIManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListNewsActivity extends AppCompatActivity {
    private RecyclerView rv;
    private NewsAdapter adapter;
    private List<Item> itemList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_news);

        getData();

        itemList = new ArrayList<>();
        adapter = new NewsAdapter(this, itemList);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);

        rv = findViewById(R.id.rvNews);
        rv.setLayoutManager(layoutManager);
        rv.setAdapter(adapter);

        adapter.setOnClick(new NewsOnClick() {
            @Override
            public void onClickItem(int position) {
                Item item = itemList.get(position);
                Intent intent = new Intent(ListNewsActivity.this, DetailActivity.class);
                intent.putExtra("URL", item.getContent().getUrl());
                startActivity(intent);
            }
        });
    }

    private void getData() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(APIManager.SERVER_URL).addConverterFactory(GsonConverterFactory.create()).build();

        APIManager service = retrofit.create(APIManager.class);
        service.getItemListData().enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {
                if(response.body() == null ) {
                    return;
                }
                itemList = response.body();
                adapter.reloadData(itemList);
            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {
                Log.d("ListNewsActivity", "onFailure: " + t);
            }
        });
    }
}