package com.example.exercise07.interfaces;

public interface NewsOnClick {
    void onClickItem(int position);
}
