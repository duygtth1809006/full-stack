package com.example.exercise07.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.exercise07.R;
import com.example.exercise07.interfaces.NewsOnClick;
import com.example.exercise07.model.Item;

import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter {
    private Activity activity;
    private List<Item> itemList;
    private NewsOnClick onClick;

    public NewsAdapter(Activity activity, List<Item> itemList) {
        this.activity = activity;
        this.itemList = itemList;
    }

    public void setOnClick(NewsOnClick onClick) {
        this.onClick = onClick;
    }

    public void reloadData(List<Item> items) {
        this.itemList = items;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = activity.getLayoutInflater().inflate(R.layout.activity_main, parent, false);
        NewHolder holder = new NewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        NewHolder newHolder = (NewHolder) holder;
        Item item = itemList.get(position);
        newHolder.tvTitle.setText(item.getTitle());
        newHolder.tvDate.setText((item.getDate()));
        newHolder.tvContent.setText(item.getContent().getDescription());
        Glide.with(activity).load(item.getImage()).into(newHolder.ivCover);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class NewHolder extends RecyclerView.ViewHolder {
        TextView tvDate, tvTitle, tvContent;
        ImageView ivCover;

        public NewHolder(@NonNull View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvContent = itemView.findViewById(R.id.tvContent);
            ivCover = itemView.findViewById(R.id.ivCover);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClick.onClickItem(getAdapterPosition());
                }
            });
        }
    }
}
